/*jslint nomen: true, browser:true*/
/*global define, module */

(function(root, factory) {
    "use strict";

    /* CommonJS */
    if (typeof exports == 'object') module.exports = factory();

    /* AMD module */
    else if (typeof define == 'function' && define.amd) define(factory);

    /* Browser global */
    else root.regression = factory();

}(this, function () {
    "use strict";

    var regression = {

        /**
         * @typedef {object} Line
         * @property {number} slope the line slope
         * @property {number} yintercept the y-intercept of the line
         */

        /**
         * Calculate the linear regression using a list of points.
         *
         * @param {integer[2][]} points the list of point.
         * @parap {bool} horizontal true if the linear regression should use
         *      a least abscissa squares method instead of a least ordinate
         *      method.
         * @return {Line}
         */
        simpleLinearRegression: function (points, horizontal) {
            var count = points.length,
                i, point;
            var vertical = !horizontal;

            // calculates the sum on the axis
            var sumX = 0,
                sumY = 0;
            for (i = 0; i < count; i++) {
                point = points[i];
                sumX += point[0];
                sumY += point[1];
            }

            // calculate the average on the axis
            var avgX = sumX / count,
                avgY = sumY / count;

            // calculate the slope
            var slopeNumerator = 0,
                slopeDenominator = 0;
            for (i = 0; i < count; i++) {
                point = points[i];
                var xDiff = point[0] - avgX;
                var yDiff = point[1] - avgY;

                slopeNumerator   += vertical ? xDiff * yDiff : yDiff * yDiff;
                slopeDenominator += vertical ? xDiff * xDiff : xDiff * yDiff;
            }
            var slope = slopeNumerator / slopeDenominator;

            // calculate the y-intercept
            var yintercept = avgY - slope * avgX;

            return {
                slope: slope,
                yintercept: yintercept
            };
        },

        /**
         * Calculate the orthogonal linear regression using a list of points.
         *
         * @param {integer[2][]} points the list of point.
         * @return {Line}
         */
        orthogonalLinearRegression: function (points) {
            var count = points.length,
                point, i;

            // calculate the sum on the axis
            var sumX = 0,
                sumY = 0;
            for (i = 0; i < count; i++) {
                point = points[i];
                sumX += point[0];
                sumY += point[1];
            }

            // calculate the average on the axis
            var avgX = sumX / count,
                avgY = sumY / count;

            // calculate the square sums
            var sumXX = 0,
                sumYY = 0,
                sumXY = 0,
                dx, dy;
            for (i = 0; i < count; i++) {
                point = points[i];
                dx = point[0] - avgX;
                dy = point[1] - avgY;
                sumXX += dx * dx;
                sumYY += dy * dy;
                sumXY += dx * dy;
            }

            // calculate the slope of the line
            var slopeNumerator = sumYY - sumXX + Math.sqrt(((sumYY - sumXX) * (sumYY - sumXX)) + (4 * sumXY * sumXY)),
                slopeDenominator = 2 * sumXY,
                slope = slopeNumerator / slopeDenominator;

            // calculate the y-intercept
            var yintercept = avgY - slope * avgX;

            return {
                slope: slope,
                yintercept: yintercept
            };
        },

        linearRegression: function(points, type){
            type = type || "vertical";
            switch(type){
                case "orthogonal":
                    return regression.orthogonalLinearRegression(points);
                case "horizontal":
                    return regression.simpleLinearRegression(points, true);
                case "vertical":
                    return regression.simpleLinearRegression(points, false);
                default:
                    throw "Unknown regression type: " + type;
            }
        }


    };

    return regression;
}));
