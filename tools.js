/*jslint nomen: true, browser:true*/
/*global define */

define(['jquery'], function ($) {
    'use strict';

    var module = {

        /**
         * This function uses the z-index to bring a dom element into front.
         *
         * @param domElement the DOM element to bring to front
         * @param [maxZIndex] the max z-index value
         */
        bringToFront: function (domElement, maxZIndex) {
            var $domElt = $(domElement);

            // get highest z-index (ignoring all z-index higher
            // than maxZIndex if it is defined)
            var maxZ = 0;

            // loop amongst the dom element siblings
            var siblings = $domElt.siblings();
            siblings.each(function () {

                // get z-index value
                var z = parseInt($(this).css('z-index'), '');

                // register it at max if higher than last registered value
                // and in function of maxZIndex
                if (z && (!maxZIndex || z < maxZIndex)) {
                    maxZ = Math.max(maxZ, z);
                }
            });

            // set the dom element to the highest z-index
            var thisZ = maxZ + 1;
            domElement.css('z-index', thisZ);
        },


        /**
         * Get or set the center of a DOM element.
         * @param elt the element.
         * @param [x || Number[2]] the x coordinate to set or an array containing both x and y coordinate
         * @param [y] the y coordinate to set
         *
         * @returns the center (or the new center) to elt.
         */
        centerOf: function (elt, x, y) {

            var $elt = $(elt);
            var eltOffset = $elt.offset();
            var eltWidth = $elt.outerWidth();
            var eltHeight = $elt.outerHeight();

            // adapt the parameters if needed (position may be given in an array
            if (!module.isUnset(x) && !module.isUnset(x[0])) {
                y = x[1];
                x = x[0];
            }

            // calculate the center of the element (from what the user specified
            // or from the current element center)
            var center = [
                module.isUnset(x) ? (eltOffset.left + eltWidth / 2) : x,
                module.isUnset(y) ? (eltOffset.top + eltHeight / 2) : y
            ];

            // set the element center if needed
            if (module.isSet(x) || module.isSet(y)) {
                $elt.css({
                    position: 'absolute',
                    top: center[1] - eltHeight / 2,
                    left: center[0] - eltWidth / 2
                });
            }

            // return it
            return center;
        },


        /**
         * Move an element.
         * @param elt the element.
         * @param [dx || Number[2]] the difference on x coordinate or an array containing both
         * @param [dy] the differenceon y coordinate
         *
         * @returns the center (or the new center) to elt.
         */
        move: function (elt, dx, dy) {
            var $elt = $(elt),
                eltOffset = $elt.offset();

            // adapt the parameters if needed (position may be given in an array
            if (!module.isUnset(dx) && !module.isUnset(dx[0])) {
                dy = dx[1];
                dx = dx[0];
            }

            $elt.css({
                top: 0,
                left: 0
            });

            return $elt.offset({
                left: eltOffset.left + dx,
                top: eltOffset.top + dy
            });
        },

        /**
         * @param value
         * @returns true if value === null or typeof value === "undefined"
         */
        isUnset: function (value) {
            return (value === null || value === void(0));
        },

        isSet: function (value) {
            return !module.isUnset(value);
        },

        /**
         * @param x an integer
         * @returns the sign of x (1 or -1)
         */
        sign: function (x) {
            return x > 0 ? 1 : x < 0 ? -1 : 0;
        },

        fullParent: function (domElt) {
            $(domElt).css({
                position: 'absolute',
                width: '100%',
                height: '100%',
                margin: 0,
                padding: 0
            });
        },

        /**
         * Shuffle an array in place
         *
         * @param list the array to shuffle
         * @returns the array
         */
        shuffle: function (list) {
            for (var j, x, i = list.length; i; j = Math.floor(Math.random() * i), x = list[--i], list[i] = list[j], list[j] = x);
            return list;
        },


        _PREVENT_DEFAULT_HANDLERS: {
            'touchstart touchend touchcancel touchmove': function (evt) {
                evt.preventDefault();
            }
        },

        preventDefaultOnTouch: function (domElt, preventDefault) {
            if (module.isSet(preventDefault) && !preventDefault) {
                $(domElt).off(module._PREVENT_DEFAULT_HANDLERS);
            } else {
                $(domElt).on(module._PREVENT_DEFAULT_HANDLERS);
            }
        },

        _EVT_MAP: {
            'touchmove'  : { device:'touch', type:'move'  },
            'touchend'   : { device:'touch', type:'end'   },
            'touchcancel': { device:'touch', type:'end'   },
            'touchstart' : { device:'touch', type:'start' },
            'mousedown'  : { device:'mouse', type:'start' },
            'mouseup'    : { device:'mouse', type:'end'   },
            'mousemove'  : { device:'mouse', type:'move'  },
        },

        eventInfo: function (evt) {
            var evtType = evt.type || evt;
            return module._EVT_MAP[evtType];
        },

        /**
         * @param {Integer} time the number of ms to wait.
         * @return {jquery.Promise} A jquery promise that will be resolved after a given time.
         */
        wait: function (time) {
            return $.Deferred(function (dfd) {
                setTimeout(dfd.resolve, time);
            }).promise();
        },

        /**
         * Used to get an asynchroneously resolved promise or/and execute a function asynchroneously.
         *
         * @param {function} [f] A function to be executed before the resolution of the promise.
         * This parameter can be replaced by the async parameter if only one argument is provided.
         * @param {boolean} [async] if set to true, the function (if provided) will be
         * (immediately) synchroneously executed and the returned promise will be already resolved.
         * @return {jquery.Promise} An asynchroneously resolved promise (if a function is provided, the promise is
         * resolved just after its execution). The arguments of the promise resolution will be the result of the
         * provided function.
         */
        async: function (f, async) {
            // polymorphism
            if (typeof f !== 'function' && !module.isSet(async)) {
                async = f;
                f = null;
            }
            async = module.isSet(async) ? async : true;

            // create the asynchroneously or synchroneously resolve promise
            var asyncProm = async ? module.wait(0) : $.Deferred(function () {
                this.resolve();
            }).promise();

            // call the callback
            if (f) asyncProm.then(f);

            // returns the promise
            return asyncProm;
        },

        restrict: function (min, x, max) {
            if (min > max) return module.restrict(max, x, min);
            else return Math.max(min, Math.min(x, max));
        },

        diff: function (a, b) {
            return Math.abs(a - b);
        },

        deepFreeze: function (obj) {
            if (!Object.isFrozen(obj)) Object.freeze(obj);
            for (var pName in obj) if (obj.hasOwnProperty(pName)) {
                var prop = obj[pName];
                if (prop !== null && (typeof prop === 'object' ||
                                      typeof prop === 'function' )) {
                    module.deepFreeze(prop);
                }
            }
        },

        clone: function (obj, deep) {
            var target = {};
            for (var i in obj) if (obj.hasOwnProperty(i)) {
                var sub = obj[i];
                target[i] = deep && typeof sub === 'object' ? module.clone(obj[i], true)
                                                            : obj[i];
            }
            return target;
        },

        defineGetter: function(obj, name, getter) {
            if (module.isUnset(getter)) {
                var targetProp = getter || '_'  + name;
                getter = function () {
                    return this[targetProp];
                };
            }
            Object.defineProperty(obj, name, { get: getter });
        },

        inherits: function(Sub, proto, Super) {
            if(!Super){
                Super = proto;
                proto = null;
            }
            return module.inheritsOf(Super, Sub, proto);
        },

        inheritsOf: function(Super, Sub, proto) {
            // copy the description of all the properties of proto
            var properties = {};
            for(var prop in proto){
                if(proto.hasOwnProperty(prop)){
                    properties[prop] = Object.getOwnPropertyDescriptor(proto, prop);
                }
            }
            // create the new object with these properties
            Sub.prototype = Object.create(Super.prototype, properties);
            // update the constructor
            Sub.prototype.constructor = Sub;
            // return the class
            return Sub;
        }

    }; // --- end of the module  ---

    module.deepFreeze(module._EVT_MAP);

    return module;

});
