define(
['jquery', './tools', 'signals'],

function factory($, tools, Signal){
    'use strict';

    var DEFAULT_CONFIG = {
        pressedClass  : 'pressed',
        minContacts   : 1,
        maxContacts   : 0,
        minEndOnButton: 'all'
    };

    var defGetter = tools.defineGetter;

    function TouchButton(dom, callback, config){
        // allow omission of the new operator
        if (!(this instanceof TouchButton)) return new TouchButton(dom, callback, config);

        config = $.extend({}, DEFAULT_CONFIG, config);

        this.callback = callback;
        this.pressedClass = config.pressedClass;

        // the minimum of contact points on the button to press it
        this.minContacts = config.minContacts;
        // the maximum of contact points on the button (exceeding it will cancel the click)
        this.maxContacts = config.maxContacts;
        // the minimum of contact points on the button when it is released
        this.minEndOnButton = config.minEndOnButton;

        // signals
        this.onPressed  = new Signal();
        this.onCanceled = new Signal();
        this.onClicked  = new Signal();
        this.onReleased = new Signal(); // dispatched when on cancel or on click

        //TODO: onTouch, onDepressed, onOut & onBack?

        // private members
        this._pressed  = false;
        this._enabled  = false;
        this._contacts = [];
        this._endedOn  = 0;
        this._ended    = 0;
        this._dom      = null; // dom will be set by attach
        // create the dom handlers
        this._handlers = this._createDOMHandlers();

        // attach the handlers to the provided dom (if any)
        if(dom) this.attach(dom);

        // onClick call button callback (if any)
        this.onClicked.add(function(evt, that){
            if(that.callback) that.callback(that.label, evt, that);
        });

        // management of the pressed class
        var that = this;
        this.onPressed.add(function(){
            if(that.pressedClass) that._dom.addClass(that.pressedClass);
        });
        this.onReleased.add(function(){
            if(that.pressedClass) that._dom.removeClass(that.pressedClass);
        });

    }
    var proto = TouchButton.prototype;

    // define read only properties
    defGetter(proto, 'dom', function() { return this._dom[0]; });
    defGetter(proto, 'pressed');
    defGetter(proto, 'enabled');

    proto.attach = function (dom) {
        if(!dom) throw 'dom is required';
        dom = $(dom);
        if(this._dom !== dom) {
            if(this._dom) this.detach();
            this._dom = dom;
            this._dom.on(this._handlers.dom);
            $('body').on(this._handlers.body);
            this._enabled = !this._dom.hasClass('disabled');
        }
    };

    proto.detach = function(){
        this._dom.off(this._handlers.dom);
        $('body').off(this._handlers.body);
        this._dom     = null;
        this._pressed = false;
        this._enabled = false;
        if(this.pressed) this._onCanceled({type: 'detached'});
    };

    proto.enable = function (val) {
        if (tools.isSet(val)) {
            if (val) {
                $(this.dom).removeClass('disabled');
            } else {
                $(this.dom).addClass('disabled');
                if(this.pressed) this._onCanceled({type: 'disabled'});
            }
            this._enabled = val;
        }
        return this._enabled;
    };

    proto._createDOMHandlers = function() {
        return {
            dom: {
                 // touchstart handler required to activate :active css pseudo class
                'touchstart mousedown': this._onPointerStart.bind(this),
                'touchend touchcancel': this._onPointerEnd.bind(this),
                'touchmove mousemove' : this._onPointerMove.bind(this)
            },
            body: {
                'mouseup': this._onPointerEnd.bind(this)
            }
        };
    };

    proto._onPointerStart = function (evt) {
        var oEvent  = evt.originalEvent || evt;
        // registers all contacts that started on the button (targetContacts is not reliable)
        this._contacts = oEvent.changedTouches ? arrayMerge(this._contacts, this._getTouchesOn(oEvent.changedTouches))
                                               : [oEvent];
        if (this.maxContacts && this._contacts.length > this.maxContacts) {
            // cancels multitouch clicks
            if (this._pressed) this._onCanceled(evt);
        } else if (this.enabled && !this.pressed
                                && (!this.minContacts || this._contacts.length >= this.minContacts)) {
            this._onPressed(evt);
        }
        evt.preventDefault();
    };

    proto._onPointerEnd = function (evt) {
        var oEvent = evt.originalEvent || evt;
        evt.preventDefault();

        // removes ended events if the button isn't pressed yet
        if(!this._pressed){
            // cases touch
            if(oEvent.changedTouches){
                for(var i = 0, n = oEvent.changedTouches.length; i < n; i++){
                    var touch = oEvent.changedTouches[i],
                        index = this._contacts.indexOf(touch);
                    if(index > -1) this._contacts.splice(index, 1);
                }
            // case mouse
            } else {
                this._contacts = [];
            }
            return;
        }

        // else count the contacts ended on the button and the contacts
        // ended outside the button

        // case touch
        if(oEvent.changedTouches) {
            for(var i = 0, n = oEvent.changedTouches.length; i < n; i++){
                var touch = oEvent.changedTouches[i];
                if(this._contacts.indexOf(touch) > -1){
                    // mark them as ended
                    this._ended++;
                    // mark them as ended on the button if it is the case
                    if(onDom([touch.clientX, touch.clientY], this.dom)) this._endedOn++;
                }
            }
        // case mouse
        } else {
            this._endedOn = 1;
            this._ended   = 1;
        }

        // trigger the click or the cancel events
        if (this._ended === this._contacts.length || oEvent.touches.length <= 0) {
            if(!this.minEndOnButton || this.minEndOnButton === 'all' && this._endedOn === this._ended
                                    || this._endedOn >= this.minEndOnButton){
                this._onClicked(evt);
            } else {
                this._onCanceled(evt);
            }
        }
    };

    proto._onPointerMove = function (evt) {
        evt.preventDefault();
    };


    proto._onPressed = function (evt) {
        this._pressed = true;
        this.onPressed.dispatch(evt, this);
    };

    proto._onClicked = function (evt) {
        this.onClicked .dispatch(evt, this);
        this.onReleased.dispatch(evt, this);
        this._reset();
    };

    proto._onCanceled = function (evt) {
        this.onCanceled.dispatch(evt, this);
        this.onReleased.dispatch(evt, this);
        this._reset();
    };

    proto._reset = function(){
        this._pressed  = false;
        this._contacts = [];
        this._endedOn  = 0;
        this._ended    = 0;
    };

    proto._getTouchesOn = function(touches){
        var targetTouches = [];
        for(var i = 0, n = touches.length; i < n; i++){
            var touch = touches[i],
                pos   = [touch.clientX, touch.clientY];
            if(onDom(pos, this.dom)) targetTouches.push(touch);
        }
        return targetTouches;
    };

    function evtOnDom(evt, dom) {
        var pos;
        var changedTouche = (evt.originalEvent || evt).changedTouches;
        // retrieve touch position
        if(changedTouche) pos = [changedTouche[0].clientX, changedTouche[0].clientY];
        // retrieve mouse position
        else              pos = [ evt.clientX, evt.clientY ];
        return onDom(pos, dom);
    }

    function onDom(pos, dom){
        var underNode = document.elementFromPoint(pos[0], pos[1]);
        var on = dom === underNode || isChild(underNode, dom);
        return on;
    }

    function isChild(/*child node*/ child, /*parent node*/ parent){ //returns boolean
        child = child.parentNode;
        while(child && child !== parent) child = child.parentNode;
        return !!child;
    }

    function arrayMerge(arr1, arr2, comp){
        comp = comp || function(x, y){ return x === y; };
        var arr3 = [];
        for(var i in arr1){
           var shared = false;
           for (var j in arr2)
               if (comp(arr2[j], arr1[i])) {
                   shared = true;
                   break;
               }
           if(!shared) arr3.push(arr1[i]);
        }
        return arr3.concat(arr2);
    }

    return TouchButton;

});
