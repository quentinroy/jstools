/*jslint nomen: true, browser:true, curly:false*/
/*global define */

(function(root, factory) {
    "use strict";

    /* CommonJS */
    if (typeof exports == 'object') module.exports = factory();

    /* AMD module */
    else if (typeof define == 'function' && define.amd) define(factory);

    /* Browser global */
    else root.geoTools = factory();

}(this, function () {
    "use strict";

    var geoTools = {

        /**
         * Calculate the distance between two n-vectors
         *
         * @param {Number[2]} pos1 the first point
         * @param {Number[2]} pos2 the second point
         * @returns {Number} the distance between the two points
         */
        dist: function (v1, v2) {
            var n = Math.min(v1.length, v2.length),
                sum = 0,
                i;
            for (i = 0; i < n; i++)
                sum += Math.pow(v1[i] - v2[i], 2);
            return Math.sqrt(sum);
        },

        /**
         * Calculate the length of a path
         *
         * @param {Number[2][]} points an array of points
         * @returns {Number} the length of the points' path
         */
        pathLength: function (points) {
            var d = 0.0,
                n = points.length;
            for (var i = 1; i < n; i++)
                d += geoTools.dist(points[i - 1], points[i]);
            return d;
        },

        /**
         * Calculate a line using two points.
         *
         * @param {Number[2]} p1 the first point
         * @param {Number[2]} p2 the second point
         * @return {{slope:Number,yIntersect:Number}} the line
         */
        pointsLine: function (p1, p2) {
            var slope = (p1[1] - p2[1]) / (p1[0] - p2[0]);
            var yIntercept = p1[1] - slope * p1[0];
            return {
                slope: slope,
                yIntercept: yIntercept
            };
        },


        /**
         * @param {Number[][]} points a list of nD points
         * @returns {Number[]} The center of gravity (so the barycentre) of the points.
         */
        barycenter: function (points) {
            var sums = [],
                result = [],
                pn = points.length,
                cn, pi, ci, p, sumci;
            for (pi = 0; pi < pn; pi++) {
                p = points[pi];
                // number of coordinates is set to the smallest
                cn = cn ? Math.min(cn, p.length) : p.length;
                for (ci = 0; ci < cn; ci++) {
                    sumci = sums[ci] || 0;
                    sums[ci] = sumci + p[ci];
                }
            }
            for (ci = 0; ci < cn; ci++) {
                result[ci] = sums[ci] / pn;
            }
            return result;
        },

        toDeg: function (radAngle) {
            return radAngle * 180 / Math.PI;
        },

        toRad: function (degAngle) {
            return degAngle * Math.PI / 180;
        },

        toDirectedAngle: function (angle, units) {
            var circle = geoTools._unitCircle(units);
            angle = angle % circle;
            if (angle > (circle / 2)) angle -= circle;
            return angle;
        },

        /**
         * @param {Number[]} A list of angles in radians.
         * @returns the average of theses angles.
         */
        avgAngle: function (angles) {
            var n      = angles.length,
                sumSin = 0,
                sumCos = 0,
                angle, i;
            for (i = 0; i < n; i++) {
                angle = angles[i];
                sumSin += Math.sin(angle);
                sumCos += Math.cos(angle);
            }
            return Math.atan2(sumSin / n, sumCos / n);
        },

        _unitCircle: function (units) {
            var undefinedVar;
            switch (units) {
            case 'deg':
            case 'degrees':
                return 360;
            case 'rad':
            case 'radians':
            case undefinedVar:
            case null:
                return Math.PI * 2;
            default:
                throw 'Unknown units type: ' + units;
            }
        },

        angleDist: function (angle1, angle2, units) {
            var circle = geoTools._unitCircle(units);

            var d1 = Math.abs(angle1 - angle2),
                d2 = Math.abs(angle1 + circle - angle2),
                d3 = Math.abs(angle1 - angle2 - circle);
            return Math.min(Math.min(d1, d2), d3);
        },


        /**
         * Resample a path.
         *
         * @param points the points of the path (warning, this array is going to be modified).
         * @param n the number of point required in the output array
         * @retunrs a path resampled
         */
        resamplePath: function (points, n) {
            //FIXME: avoid to change the points array
            n = n || points.length;
            var intervalLength = geoTools.pathLength(points) / (n - 1), // interval length
                currentLength = 0.0,
                newpoints = [points[0], ],
                i, d, px, py, p;

            for (i = 1; i < points.length; i++) {
                d = geoTools.dist(points[i - 1], points[i]);
                if ((currentLength + d) >= intervalLength) {
                    px = points[i - 1][0] + ((intervalLength - currentLength) / d) * (points[i][0] - points[i - 1][0]);
                    py = points[i - 1][1] + ((intervalLength - currentLength) / d) * (points[i][1] - points[i - 1][1]);
                    p = [px, py];
                    newpoints.push(p);
                    points.splice(i, 0, p); // insert 'p' at position i in points s.t. 'p' will be the next i
                    currentLength = 0.0;
                } else {
                    currentLength += d;
                }
            }
            if (newpoints.length == n - 1) // sometimes we fall a rounding-error short of adding the last point, so add it if so
                newpoints.push([points[points.length - 1][0], points[points.length - 1][1]]);
            return newpoints;
        }

    };

    return geoTools;

}));
