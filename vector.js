/**
 * @file vector library
 * @author Quentin Roy <quentin.roy@ge.com>
 * @version 0.6
 */

/*jslint nomen: true, browser:true, curly:false*/
/*global define, module */

(function(factory, root) {
    "use strict";

    /* CommonJS */
    if (typeof exports == 'object') module.exports = factory();

    /* AMD module */
    else if (typeof define == 'function' && define.amd) define(factory);

    /* Browser global */
    else root.vector = factory();

}(function() {
    "use strict";


    /**
     * Represents a vector. This constructor can be called without the 'new' instruction.
     *
     * @constructor
     * @param {Number[]|Number} vectorArray An array containing the dimensions of the vector.
     * The dimensions of the vector can also be given as arguments (without array).
     */
    function vector(vectorArray) {
        /*jshint validthis:true, newcap: false */

        // allow to pass vector values as arguments (not in an array)
        if (!isNaN(vectorArray)) vectorArray = Array.prototype.slice.call(arguments);

        // allow omission of the new operator
        if (!(this instanceof vector)) return new vector(vectorArray);

        // copy the values
        var i, n = vectorArray.length;
        for (i = 0; i < n; i++) this[i] = vectorArray[i];
        this._length = n;
    }

    vector.prototype = {

        /**
         * @deprecated
         */
        get values() {
            return this;
        },

        get length() {
            return this._length;
        },

        set length(newLength) {
            var oldLength = this._length,
                i;
            for (i = oldLength; i < newLength; i++) {
                this[i] = 0;
            }
            for (i = oldLength - 1; i >= newLength; i--) {
                delete this[i];
            }
            this._length = newLength;
        },

        /**
         * @this {vector}
         * @returns {Number[]} this vector as an array
         */
        toArray: function() {
            var values = [],
                n = this.length,
                i;
            for (i = 0; i < n; i++) values[i] = this[i];
            return values;
        },

        /**
         * Calculate the projection of the vector on a vector.
         *
         * Works only for 2D vectors.
         *
         * @param {vector|Number[2]} vect a vector
         * @this {vector}
         * @returns {vector} the resulting vector
         */
        projection: function(vect) {
            vect = (vect instanceof vector) ? vect : vector(vect);
            var unitVect = vect.unit(),
                scalarProjection = this.scalarProjection(vect),
                proj = unitVect.scalarProduct(scalarProjection);
            return proj;
        },

        /**
         * Works only for 2D vectors.
         *
         * @param {vector|Number[2]} vect a vector
         * @this {vector}
         * @returns {Number} The scalar projection of this on the given vector
         */
        scalarProjection: function(vect) {
            vect = (vect instanceof vector) ? vect : vector(vect);
            return this.dotProduct(vect) / vect.norm();
        },

        /**
         * @returns {Number} the norm of the vector.
         * @this {vector}
         */
        norm: function() {
            var sum = 0,
                i, c;
            for (i = 0; i < this.length; i++) {
                c = this[i];
                sum += c * c;
            }
            return Math.sqrt(sum);
        },

        /**
         * @returns a clone of this vector
         * @this {vector}
         */
        copy: function() {
            return vector(this);
        },

        /**
         * @returns {vector} the unit vector of this vector
         * @this {vector}
         */
        unit: function() {
            return this.scalarProduct(1 / this.norm());
        },

        /**
         * @returns {vector} the opposite vector
         * @this {vector}
         */
        inverted: function() {
            return this.scalarProduct(-1);
        },

        /**
         * @param {Number} scalar the scalar
         * @this {vector}
         * @returns {vector} the product of this vector by a scalar
         */
        scalarProduct: function(scalar) {
            var newVect = this.copy(),
                length = newVect.length;
            for (var i = 0; i < length; i++) newVect[i] *= scalar;
            return newVect;
        },

        /**
         * @param {vector|Number[2]} vect a vector
         * @this {vector}
         * @returns {Number} The dot product of this with the given vector
         */
        dotProduct: function(vector) {
            var dot = 0,
                n = this.length;
            if (n != vector.length) throw "The vectors must have similar lengths";
            for (var i = 0; i < n; i += 1) dot += this[i] * vector[i];
            return dot;
        },

        /**
         * Only works for 2D vectors.
         *
         * @param {vector|Number[2]} vect a vector
         * @this {vector}
         * @returns {Number} The signed angle in radian from this vector to the given vector.
         */
        angleFrom: function(vect) {
            var dotProduct = this.dotProduct(vect),
                perpDot = this[1] * vect[0] - this[0] * vect[1];
            return Math.atan2(perpDot, dotProduct);
        },

        /**
         * Returns the trigonometric angle of the vector
         */
        angle: function() {
            return this.angleFrom([1, 0]);
        },

        /**
         * @param {Number[]|vector} vect the vector to add
         * @param [force] Force the addition of a vector with a != length
         * @this {vector}
         * @returns {vector} The addition of this vector with another
         */
        add: function(vect, force) {
            var result = this.copy(),
                length = this.length;
            if (!force && length != vect.length) throw "The vectors must have similar lengths";
            for (var i = 0; i < length; i += 1) result[i] += (vect[i] || 0);
            return result;
        },

        /**
         * @param {Number[]|vector} vect the vector to substract
         * @param [force] Force the addition of a vector with a != length
         * @this {vector}
         * @returns {vector} The difference of this vector with another
         */
        substract: function(vect, force) {
            return vector(vect).scalarProduct(-1).add(this, force);
        },

        /**
         * Works only for 2D vectors.
         *
         * @returns {vector} the clockwised rotated normal vector of this vector
         */
        normalVectorClockwise: function() {
            if (this.length > 2) throw "Normal vector is only available for 2D vectors.";
            return vector(-this[1], this[0]);
        },

        /**
         * Works only for 2D vectors.
         *
         * @returns {vector} the clockwised rotated normal vector of this vector
         */
        normalVector: function() {
            return this.normalVectorClockwise();
        },

        /**
         * Works only for 2D vectors.
         *
         * @returns {vector} the anticlockwised rotated normal vector of this vector
         */
        normalVectorAnticlockwise: function() {
            return this.normalVectorClockwise().scalarProduct(-1);
        },

        /**
         * Return a rotated copy of the vector.
         * @param {Number} angle the angle
         */
        rotated: function(angle) {
            return vector([
                this[0] * Math.cos(angle) - this[1] * Math.sin(angle),
                this[0] * Math.sin(angle) + this[1] * Math.cos(angle)
            ]);
        },

        toString: function() {
            return 'vector (' + this.toArray().join(',') + ')';
        },

    };


    /**
     * @param {Number[2]} a first point
     * @param {Number[2]} b second point
     * @returns {vector} the vector between the two points
     */
    vector.from2points = function(a, b) {
        return vector(b).substract(a);
    };

    return vector;

}, this));
