define(["./vector"], function (vector) {
    "use strict";

    function CircleFitter(points) {
        /** @private */
        this._points = [];

        // == center stuffs ==
        /** @private */
        this._centerUpdated = true;
        /** @private */
        this._lastCenterUpdateLength = 0;
        /** @private */
        this._angleCombinations = 0;
        /** @private */
        this._centerSum = [0, 0];
        /** @private */
        this._center = [];
        /** @private */
        this._pParts = [];
        this._pCombis = [];

        // == radius stuffs ==
        /** @private */
        this._radiusAngleUpdated = true;
        /** @private */
        this._rHat = null;
        /** @private */
        this._dists = null;

        // == angle stuffs ==
        /** @private */
        this._rotationAngle = 0;

        // == standard deviation stuffs ==
        /** @private */
        this._stdDevUpdated = true;
        /** @private */
        this._stdDev = null;

        this._newPoints = [];
        this._removedPoints = [];

        if (points) this.addPoints(points);
    }

    CircleFitter.prototype = {


        /**
         * @returns {Number} The radius of the circle (calculates it if needed).
         */
        radius: function () {
            if (!this._radiusAngleUpdated) this._updateRadiusAngle();
            return this._rHat;
        },


        /**
         * @returns {Number} The angle of the rotation around the circle (calculates it if needed).
         */
        rotationAngle: function () {
            if (!this._radiusAngleUpdated) this._updateRadiusAngle();
            return this._rotationAngle;
        },

        /**
         * @returns {Number} The number of revolutions of the circle (calculates it if needed).
         */
        revolutions: function () {
            return Math.abs(this.rotationAngle() / (2 * Math.PI));
        },

        /**
         * Return the direction of the circle rotation. It calculates it if not yet updated.
         * The values of the direction are:
         *      1 for trig direction,
         *      -1 for antitrig direction,
         *      0 if there is no rotation around the circle.
         *
         * @returns {Number} The direction of the circle rotation (calculates it if needed).
         */
        direction: function () {
            var angle = this.rotationAngle();
            return angle ? angle / Math.abs(angle) : 0;
        },

        /**
         * @returns {Number[2]} The center of the circle (calculates it if needed).
         */
        center: function () {
            if (!this._centerUpdated) this._updateCenter();
            return this._center;
        },

        /**
         * @returns {Number} The standard deviation of the circle fitting (calculates it if needed).
         */
        stdDev: function () {
            if (!this._stdDevUpdated) this._updateStdDev();
            return this._stdDev;
        },

        /**
         * Get all the features of the circle.
         * Computes everything not already updated and thus may be used to ensure all
         * circle features are updated to fasten future access.
         *
         * @returns {{center:Number[2], radius:Number, stdDev:Number, angle:Number, revolutions:Number, direction:Number}}
         */
        features: function () {
            return {
                center: this.center(),
                radius: this.radius(),
                stdDev: this.stdDev(),
                rotationAngle: this.rotationAngle(),
                revolutions: this.revolutions(),
                direction: this.direction()
            };
        },

        /**
         * Alias for {@link features}.
         */
        updateAll: function () {
            return this.features();
        },

        /**
         * Add a point at the end of the circle stroke.
         * It will be considered as the last point of the stroke.
         * This removes all cached values.
         *
         * @param {Number[2]} The point to add.
         */
        addPoint: function (p) {
            this.addPoints([p]);
        },

        /**
         * Add a series of points at the end of the circle stroke.
         * This removes all cached values.
         *
         * @param {Number[2][]} The points to add.
         */
        addPoints: function (points) {
            this._newPoints = this._newPoints.concat(points);

            this._centerUpdated = false;
            this._radiusAngleUpdated = false;
            this._stdDevUpdated = false;
        },

        /**
         * Remove some points at the beginning of the circle's stroke.
         * @param {Number} [number=1] The number of points to remove.
         */
        removeStart: function (number) {
            number = (number || number === 0) ? number : 1;
            var p = this._points.splice(0, number);
            this._removedPoints = this._removedPoints.concat(p);

            this._centerUpdated = false;
            this._radiusAngleUpdated = false;
            this._stdDevUpdated = false;
        },

        length: function () {
            return this._points.length + this._newPoints.length;
        },

        /**
         * @private
         */
        _pointCenterWeight: function (point, otherPoints) {
            var centerSum = [0, 0],
                n = 0,
                length = otherPoints.length;

            for (var j = 1; j < length; j++) {
                var pj = otherPoints[j];
                for (var k = 0; k < j; k++) {
                    var pk = otherPoints[k];
                    // compute the triangle circumcenter
                    var cc = this._circumcenter(point, pj, pk);
                    if (cc !== null) {
                        centerSum[0] += cc[0];
                        centerSum[1] += cc[1];
                        n += 1;
                    }
                }
            }

            return {
                centerSum: centerSum,
                combinations: n
            };
        },

        /**
         * @private
         *
         * Recalculate the center of the points.
         */
        _updateCenter: function () {
            var p, pi, weight,
                removedPointsCount = this._removedPoints.length;
            if (removedPointsCount < this._points.length) {
                var pointsAndRemoved = this._points.concat(this._removedPoints);
                for (pi = 0; pi < removedPointsCount; pi++) {
                    p = pointsAndRemoved.pop();
                    weight = this._pointCenterWeight(p, pointsAndRemoved);
                    this._centerSum[0] -= weight.centerSum[0];
                    this._centerSum[1] -= weight.centerSum[1];
                    this._angleCombinations -= weight.combinations;
                }
            } else if (removedPointsCount) {
                this._clearCache();
            }
            for (pi in this._newPoints) {
                p = this._newPoints[pi];
                weight = this._pointCenterWeight(p, this._points);
                this._centerSum[0] += weight.centerSum[0];
                this._centerSum[1] += weight.centerSum[1];
                this._angleCombinations += weight.combinations;
                this._points.push(p);
            }
            this._removedPoints = [];
            this._newPoints = [];
            this._center[0] = this._centerSum[0] / this._angleCombinations;
            this._center[1] = this._centerSum[1] / this._angleCombinations;
        },

        _clearCache: function () {
            this._newPoints = this._points.concat(this._newPoints);
            this._removedPoints = [];
            this._points = [];
            this._centerSum = [0, 0];
            this._angleCombinations = 0;
            this._centerUpdated = false;
            this._radiusAngleUpdated = false;
            this._stdDevUpdated = false;
        },

        /** @private */
        _circumcenter: function (pI, pJ, pK) {

            var dIJ = [pJ[0] - pI[0], pJ[1] - pI[1]],
                dJK = [pK[0] - pJ[0], pK[1] - pJ[1]],
                dKI = [pI[0] - pK[0], pI[1] - pK[1]];
            var sqI = pI[0] * pI[0] + pI[1] * pI[1],
                sqJ = pJ[0] * pJ[0] + pJ[1] * pJ[1],
                sqK = pK[0] * pK[0] + pK[1] * pK[1];

            // determinant of the linear system: 0 for aligned points
            var det = dJK[0] * dIJ[1] - dIJ[0] * dJK[1];

            if (Math.abs(det) < 1.0e-10) {
                // points are almost aligned, we cannot compute the circumcenter
                return null;
            }

            var x = (sqI * dJK[1] + sqJ * dKI[1] + sqK * dIJ[1]) / (2 * det),
                y = -(sqI * dJK[0] + sqJ * dKI[0] + sqK * dIJ[0]) / (2 * det);
            return [x, y];
        },


        /** @private */
        _updateRadiusAngle: function () {
            var center = this.center(),
                rHat = 0,
                angle = 0,
                dists = [],
                lastVect = null;
            for (var i in this._points) {
                var point = this._points[i],
                    vect = vector.from2points(center, point),
                    dist = vect.norm(),
                    thisAngle = lastVect ? vect.angleFrom(lastVect) : 0;
                lastVect = vect;
                angle += thisAngle;
                dists.push(dist);
                rHat += dist;
            }
            this._rHat = rHat / this._points.length;
            this._dists = dists;
            this._rotationAngle = angle;

            this._radiusAngleUpdated = true;
        },


        /** @private */
        _updateStdDev: function () {
            var mean = this.radius(),
                variance = 0;

            for (var i in this._points) {
                var dist = this._dists[i];
                var diff = mean - dist;
                variance += diff * diff;
            }

            variance /= this._points.length;
            this._stdDev = Math.sqrt(variance);

            this._stdDevUpdated = true;
        },
    };


    /**
     * Fit a circle from a serie of points.
     *
     * @params Number[2][] point
     *      An array containing the points.
     *
     * @returns {{center:Number[2], radius:Number, stdDev:Number, angle:Number, revolutions:Number, direction:Number}}
     *      The features of the circle
     */
    CircleFitter.fit = function (points) {
        return new CircleFitter(points).features();
    };

    return CircleFitter;

});