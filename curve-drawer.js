/*jslint nomen: true, browser:true*/
/*global define */

define(["snap-svg", "jquery"], function (Snap, $) {
    "use strict";


    /**
     * This small class is dedicated to the string path creation
     * @param lineType the type of path: "L" (for simple line) or "R" (for Catmull-Rom curves)
     */
    function PathStringBuilder(lineType) {

        var _this = this;

        lineType = lineType || "R";
        if (lineType !== "L" && lineType !== "R") {
            throw "Unknown line type: " + lineType;
        }

        // the path string
        this._pathString = "";

        // this array stores the points in the form of an object with id and pos properties
        this._points = [];

        this._numPhantoms = 0;


        /**
         * Add a point to the string path
         * @param newPoint a point on the form [x, y]. It can be null (phantom points).
         * @param pointid the id of the point. It should be unique.
         */
        this.addPoint = function (newPoint, pointid) {
            var numPoint = _this._points.length - this._numPhantoms;

            _this._points.push({
                id: pointid,
                pos: newPoint // if null, this is a phantom
            });

            // if the new points is not a phantom
            if (newPoint) {

                if (numPoint <= 3) {
                    this._refreshPathString();

                    // from 4th points, we can just add the point coordinate at the end of the string
                } else {
                    _this._pathString += " " + newPoint[0] + " " + newPoint[1];
                }

            } else {
                _this._numPhantoms += 1;
            }
        };


        /**
         * Remove all the point until the point pointId (excluded)
         */
        this.removePoints = function (pointId) {
            var targetPoint = null,
                removedPhantoms = 0,
                i,
                point,
                pointCount = _this._points.length;

            // find the target point and remove all the previous ones
            for (i = 0; i < pointCount; i += 1) {
                point = _this._points[i];
                if (point.id === pointId) {

                    // if this is the last point, clear everything and stop
                    if (i === (_this._points.length - 1)) {
                        _this.reset();
                        return;
                    }

                    // record the target point
                    targetPoint = point;

                    // remove all the previous ones
                    _this._points.splice(0, i);
                    break;
                } else if (!point.pos) {
                    // record all the removed phantom points
                    removedPhantoms += 1;
                }
            }

            if (!targetPoint) {
                throw "Point " + pointId + " not found.";
            }

            // we update the number of phantom points only if no exception
            // has been raised
            this._numPhantoms = this._numPhantoms - removedPhantoms;

            _this._refreshPathString();

        };


        /**
         * Recreate the path string from scratch using the list of points
         */
        this._refreshPathString = function () {
            var numPoint = _this._points.length,
                numRealPoint = numPoint - this._numPhantoms,
                modifier = "";

            if (numRealPoint <= 0) {
                _this._pathString = "";
                return;
            } else if (numRealPoint === 2) {
                modifier = " L";
            } else if (numRealPoint >= 3) {
                // Catmull-Rom type of path does not work with less than 3 points.
                modifier = lineType;
            }

            var foundAReal = false;


            _this._pathString = "M";

            for (var i = 0; i < numPoint; i++) {
                var thisPoint = _this._points[i];
                if (thisPoint.pos) {
                    _this._pathString += " " + thisPoint.pos[0] + " " + thisPoint.pos[1];
                    if (!foundAReal) {
                        this._pathString += (" " + modifier);
                        foundAReal = true;
                    }
                }
            }
        };

        this.pointsCount = function () {
            return _this._points.length;
        };

        this.lastPoint = function () {
            return _this._points[_this.points.length - 1];
        };

        /**
         * @return the pathString
         */
        this.pathString = function () {
            return _this._pathString;
        };

        /**
         * Reset the PathStringBuilder
         */
        this.reset = function () {
            _this._pathString = "";
            _this._points = [];
        };
    }


    function CurveDrawer(snapCanvas, strokeColor, strokeWidth) {

        // Public parameters
        this._strokeColor = strokeColor || 'black';
        this._strokeWidth = strokeWidth || 10;

        this._path = null;
        this._psBuilder = new PathStringBuilder();
        this._paper = null;
        this._parent = null;
        this._givenPaper = null;


        // if (snapCanvas instanceof HTMLElement) this._parent = snapCanvas;
        this._givenPaper = snapCanvas;

        var _this = this;

        this.addPoint = function (posX, posY, id) {

            var point = null;
            if (posX) {
                if (posY) {
                    point = [posX, posY];
                } else {
                    point = [posX[0], posX[1]];
                }
            }

            // _this._points.push(point);
            _this._psBuilder.addPoint(point, id);
            this._updatePath();
        };

        this.remove = function (id) {
            _this._psBuilder.removePoints(id);
            this._updatePath();
        };

        this._updatePath = function () {
            // get the path's string SVG representation
            var pathString = _this._psBuilder.pathString();

            // debug stuff
            // console.log("pathString: " + pathString);

            if (!pathString || pathString === "") {
                if (_this._path) _this._path.remove();
                if (_this._paper && !_this._givenPaper) _this._paper.remove();
                _this._path = null;
                _this._paper = null;
            }
            // create a new path if it does not exist
            else if (!_this._path) {
                _this._paper = _this._givenPaper ? _this._givenPaper : Snap(_this._parent);
                _this._createNewPath(pathString);
            }
            // else update it
            else _this._path.attr({
                    path: pathString
                });
        };

        this.clear = function () {
            if (_this._path) _this._path.remove();
            if (_this._paper && !_this._givenPaper) _this._paper.remove();
            _this._path = null;
            _this._paper = null;
            _this._psBuilder.reset();
        };

        this.isEmpty = function () {
            return _this._psBuilder.pointsCount() === 0;
        };


        this.setStrokeColor = function (strokeColor) {
            _this.updatePathAttr(strokeColor);
        };
        
        this.setStrokeWidth = function(strokeWidth) {
            _this.updatePathAttr(strokeWidth);
        };
        
        this.updatePathAttr = function (strokeColor, strokeWidth) {
            _this._strokeColor = strokeColor || _this._strokeColor;
            _this._strokeWidth = strokeWidth || _this._strokeWidth;
            if (_this._path) {
                _this._path.attr({
                    'stroke': _this._strokeColor,
                    "stroke-width": _this._strokeWidth,
                    'stroke-linecap': 'round',
                    "fill": "none"
                });
            }
        };


        this._createNewPath = function (pathString) {
            _this._path = _this._paper.path(pathString);
            _this.updatePathAttr();
            $(_this._path.node)
                .css({
                'pointer-events': 'none'
            });
        };

    }

    return CurveDrawer;

});