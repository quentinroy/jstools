requirejs.config({
    enforceDefine: true,
    paths: {
        'jquery': ['http://code.jquery.com/jquery-2.0.3.min', '../libs/jquery-2.0.3'],
        'snap-svg': '../libs/snap.svg',
        'jstools': '../../src',
    }
});

define(["jstools/circle-fitter",
        "jstools/tools",
        "jstools/curve-drawer",
        "jstools/geoTools",
        "jquery",
        "snap-svg"],
    function (circleFitter,
        tools,
        CurveDrawer,
        geoTools,
        $,
        Snap) {
        "use strict";

        var $mainDiv = $("#main-div"),
            snapSvg = Snap("#canvas"),
            mousedown = false,
            strokeDrawer = new CurveDrawer(snapSvg, "gray"),
            points = [],
            circleNodes = [],
            originalPoints;


        function addPoint(x, y) {
            strokeDrawer.addPoint(x, y);
            points.push([x, y]);
        }

        function reset() {
            points = [];
            originalPoints = null;
            strokeDrawer.clear();
            removeCircles();
        }

        function removeCircles() {
            var i, circleNode, n = circleNodes.length;
            for (i = 0; i < n; i++) {
                circleNode = circleNodes[i];
                circleNode.remove();
            }
            circleNodes = [];
        }

        function circlesFit() {
            var i = 0,
                fitter, fitterName, color;

            for (fitterName in fitters) {
                if (fitters.hasOwnProperty(fitterName)) {
                    fitter = fitters[fitterName];
                    color = fittersColors[i];
                    circleDraw(fitter(), color);
                    i++;
                }
            }
        }

        function drawLegend() {
            var i=0, fitterName, color,
                legendDiv = $('#legend'),
                legendItem;
            for (fitterName in fitters) {
                if (fitters.hasOwnProperty(fitterName)) {
                    color = fittersColors[i];
                    legendItem = $('<span class="legend-item"></span>');
                    legendItem.appendTo(legendDiv);
                    legendItem.html(fitterName);
                    legendItem.css('color', color);
                    i++;
                }
            }

        }

        function circleDraw(circleAttr, color) {
            var circle = snapSvg.el("circle").attr({
                cx: circleAttr.center[0],
                cy: circleAttr.center[1],
                r: circleAttr.radius,
                stroke: color,
                fill: 'none',
                "stroke-opacity": 0.5,
                "stroke-width": 4
            });
            circleNodes.push(circle);
        }

        $mainDiv.on({
            "mousedown": function (evt) {
                reset();
                addPoint(evt.pageX, evt.pageY);
                mousedown = true;
            },

            "mousemove": function (evt) {
                if (mousedown) {
                    addPoint(evt.pageX, evt.pageY);
                }
            },

            "mouseup": function () {
                if (mousedown) {
                    circlesFit();
                    mousedown = false;
                }
            }
        });

        // FITTERS
        // -------


        var fitters = {

            'Least Square': function () {
                return circleFitter.fit(points.slice());
            },

            'Resampled Least Square': function () {
                var resampledPoints = geoTools.resamplePath(points.slice());
                return circleFitter.fit(resampledPoints);
            },

            'Resampled Barycenter Centered': function () {
                var resampledPoints = geoTools.resamplePath(points.slice()),
                    i, n = resampledPoints.length,
                    point,
                    xSum = 0,
                    ySum = 0;
                for (i = 0; i < n; i++) {
                    point = resampledPoints[i];
                    xSum += point[0];
                    ySum += point[1];
                }

                var barycenter = [xSum / n, ySum / n],
                    distSum = 0;
                for (i = 0; i < n; i++) {
                    point = resampledPoints[i];
                    distSum += geoTools.dist(point, barycenter);
                }
                return {
                    center: barycenter,
                    radius: distSum / n
                };
            },

            '20 pts Resampled Least Square': function () {
                var resampledPoints = geoTools.resamplePath(points.slice(), 20);
                return circleFitter.fit(resampledPoints);
            }
        },

            fittersColors = ['blue', 'red', 'green', 'pink'];


        // INITIALIZATION
        // --------------


        reset();
        drawLegend();

    });