requirejs.config({
    enforceDefine: true,
    paths: {
        'jquery': '../libs/jquery-2.0.3',
        'snap-svg': '../libs/snap.svg',
        'jstools': '../../src',
    }
});

define(["jstools/tools", "jstools/curve-drawer", "jquery", "snap-svg"], function (tools, CurveDrawer, $, Snap) {
    "use strict";

    var $mainDiv = $("#main-div"),
        snapSvg = Snap("#canvas"),
        mousedown = false,
        strokeDrawer = new CurveDrawer(snapSvg, "gray"),
        circle = snapSvg.circle(0, 0, 0),
        pointNodes = [];

    function FitWorker(){
        var callbacks = {};
        var fitWorker = new Worker("fitWorker.js");

        this.send = function(type, data){
            fitWorker.postMessage({type: type, data: data});
        };

        this.on = function(type, callback){
            callbacks[type] = callbacks.type || [];
            callbacks[type].push(callback)
        };

        fitWorker.onmessage = function(evt){
            var msg = evt.data;
            var targetCallbacks = callbacks[msg.type];
            if(targetCallbacks){
                targetCallbacks.forEach(function(c){
                    c(msg.data);
                });
            }
        };
    }

    var fitWorker = new FitWorker();

    function addPoint(x, y) {
        strokeDrawer.addPoint(x, y);
        fitWorker.send("newPoint", [x, y]);
    }

    function reset() {
        fitWorker.send("clear");
        removePointNodes();
        strokeDrawer.clear();
        circle.attr({
            fill: "none",
            stroke: "none"
        });
    }

    function removePointNodes() {
        pointNodes.forEach(function(pointNode){
            pointNode.remove();
        });
        pointNodes = [];
    }

    function drawPoints(pointsToDraw) {
        pointsToDraw = pointsToDraw || points;
        removePointNodes();
        pointNodes = pointsToDraw.map(function(point){
            return snapSvg.circle(point[0], point[1], 3);
        });
    }

    function updateCircle(circleAttr) {
        if(!isNaN(circleAttr.center[0]) && !isNaN(circleAttr.center[1]) && !isNaN(circleAttr.radius)){
            circle.attr({
                cx: circleAttr.center[0],
                cy: circleAttr.center[1],
                r: circleAttr.radius,
                fill: "blue",
                "fill-opacity": 0.1
            });
        }
    }

    var refreshing = false;
    var valid = true;
    function refresh(){
        if(!refreshing){
            refreshing = true;
            valid = true;
            fitWorker.send("fit");
        } else {
            valid = false;
        }
    }
    fitWorker.on("fit", function(result){
        window.requestAnimationFrame(function(){
            updateCircle(result.circle);
            drawPoints(result.points);
            refreshing = false;
            if(!valid){
                refresh();
            }
        });
    });
    fitWorker.on("fitFailed", function(){
        refreshing = false;
        if(!valid){ refresh(); }
    });

    $mainDiv.on({
        "mousedown": function (evt) {
            reset();
            addPoint(evt.pageX, evt.pageY);
            mousedown = true;
            // invalidate();
        },

        "mousemove": function (evt) {
            if (mousedown) {
                addPoint(evt.pageX, evt.pageY);
                refresh();
            }
        },

        "mouseup": function () {
            if (mousedown) {
                mousedown = false;
                refresh();
            }
        }
    });

    $("#controls input[name=regression]").change(function(evt){
        fitWorker.send("setMethod", evt.target.value);
        refresh();
    });


    fitWorker.on("log", function(data){ console.log.apply(console, data); });
    fitWorker.on("ready", function(){
        fitWorker.send("setMethod", $("#controls input[name=regression]:checked")[0].value);
    });
    reset();
});