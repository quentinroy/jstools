importScripts('../libs/require.js');
require({ baseUrl: "../../src" }, ["circle-fitter", "geoTools"], function(circleFitter, geoTools) {

    var method;
    var originalPoints = [];

    function send(type, data){
        postMessage({type: type, data: data});
    }

    function log(){
        send("log", Array.from(arguments));
    }

    function circleFit(fail, success) {
        if(originalPoints.length < 3){
            fail("not enough points");
            return;
        }
        switch(method){
            case "original":
                var points = originalPoints;
                break;
            case "resampled":
                var points = geoTools.resamplePath(originalPoints.slice());
                break;
            case "resample30":
                var points = geoTools.resamplePath(originalPoints.slice(), 30);
                break;
            default:
                fail("unknown method: " + method);
                return;
        }
        success({ circle: circleFitter.fit(points), points: points });
        return;
    }

    handlers = {
        newPoint: function(point){
            originalPoints.push(point);
        },
        clear: function(){
            originalPoints = [];
        },
        setMethod: function(newMethod){
            method = newMethod;
        },
        fit: function(){
            circleFit(function(msg){
                send("fitFailed", msg);
            }, function(result){
                send("fit", result);
            });
        }
    };

    // On définit la fonction à appeler lorsque la page principale nous sollicite
    this.addEventListener('message', function(evt){
        var msg = evt.data;
        handlers[msg.type](msg.data);
    });

    send("ready");

});
