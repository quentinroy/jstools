requirejs.config({
    enforceDefine: true,
    paths: {
        'jquery': ['http://code.jquery.com/jquery-2.0.3.min', '../libs/jquery-2.0.3'],
        'snap': '../libs/snap.svg',
        'vector': '../../src/vector'
    }
});

define(["vector", "jquery", "snap"], function (vector, $, Snap) {
    "use strict";

    var p1,
        p2,
        l1,
        l2,
        strokeWidth = 10,
        $canvasDiv = $("#canvas"),
        paper = Snap($canvasDiv[0]),
        // oCircle,
        $angleDiv = $("#angle-result"),
        currentWidth = $canvasDiv.width(),
        currentHeight = $canvasDiv.height();


    function linePath(p1, p2) {
        return ["M", p1[0], p1[1], "L", p2[0], p2[1]].join(" ");
    }

    function updateP1() {
        if (p1) {
            var pathStr = getPPathStr(p1);
            if (!l1) {
                l1 = paper.path(pathStr);
                l1.attr({
                    "stroke-linecap": "round",
                    "stroke": "#2A2AFF",
                    "stroke-width": strokeWidth,
                    opacity: 0.4
                });
                l1.toBack();
                if (l2) l1.insertBefore(l2);
            } else l1.attr({
                    path: pathStr
                });
        } else if (l1) {
            l1.remove();
            l1 = null;
        }
    }

    function getPPathStr(p) {

        var o = pageCenter(),
            poVect = vector(p).substract(o).unit(),
            // the vector must be longer than canvas width and canvas height
            longPoVect = poVect.scalarProduct($canvasDiv.width()+$canvasDiv.height()),
            extrPoint = longPoVect.add(o).values,
            pathStr = linePath(o, extrPoint);
        return pathStr;
    }

    function pageCenter() {
        var canvasOffset = $canvasDiv.offset();
        return [$canvasDiv.width() / 2 + canvasOffset.left, $canvasDiv.height() / 2 + canvasOffset.top];
    }

    function updateP2() {
        if (p2) {
            var pathStr = getPPathStr(p2);
            if (!l2) {
                l2 = paper.path(pathStr);
                l2.attr({
                    "stroke-linecap": "round",
                    "stroke": "#FF2A2B",
                    "stroke-width": strokeWidth,
                    opacity: 0.4
                });
                l2.toBack();
                if (l1) l2.insertAfter(l1);
            } else l2.attr({
                    path: pathStr
                });
        } else if (l2) {
            l2.remove();
            l2 = null;
        }
    }


    function updateAngle() {
        if (p1 && p2) {
            var o = pageCenter(),
                v1 = vector(p1).substract(o),
                v2 = vector(p2).substract(o),
                angle = v2.angleFrom(v1),
                adaptedAngle = (angle / Math.PI).toFixed(2),
                text = adaptedAngle == 0 ? adaptedAngle : adaptedAngle + " π";
            $angleDiv.html(text);
        }
    }

    function update() {
        updateP1();
        updateP2();
        updateAngle();
    }


    var mouseDown = false;

    $canvasDiv.on("mousedown", function (evt) {
        p1 = [evt.originalEvent.offsetX, evt.originalEvent.offsetY];
        mouseDown = true;
        update();
    });

    $canvasDiv.on("mouseup", function () {
        mouseDown = false;
    });

    $canvasDiv.on("mousemove", function (evt) {
        p2 = [evt.originalEvent.offsetX, evt.originalEvent.offsetY];
        if (mouseDown) p1 = p2;
        update();
    });

    $(document).ready(function () {
        var canvasOffset = $canvasDiv.offset(),
            o = pageCenter();
        p1 = [o[0], canvasOffset.top];
        update();
    });

    $(window).resize(function () {
        var lastWidth = currentWidth,
            lastHeight = currentHeight;
        currentWidth = $canvasDiv.width();
        currentHeight = $canvasDiv.height();
        paper.setSize(currentWidth, currentHeight);
        p1[0] += (currentWidth - lastWidth)/2;
        p1[1] += (currentHeight - lastHeight)/2;
        p2[0] += (currentWidth - lastWidth)/2;
        p2[1] += (currentHeight - lastHeight)/2;
        update();
    });

});